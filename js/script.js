"use strict";
/*
Краще використовувати input події, оскільки користувач може використовувати альтернативні методи введення тексту, такі як системи ручного написання (на графічних планшетах), голосового введення, copy-paste.
*/
window.addEventListener("keydown", event => {
    document.querySelectorAll(".btn").forEach(item => event.code === item.dataset.code ? item.classList.add("current") : item.classList.remove("current")
            );
});
